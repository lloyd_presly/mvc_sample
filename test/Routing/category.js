
module.exports = function(request, url) {
	describe('categories', function() {
		it('Case1: Create Category - Duplicate entry', function(done) {
			var categoryData = {
				'name' : 'categoryNameeee',
				'categoryImage' : 'mdsgdd',
				'order' : 1
			}

			request(url)
				.post('/api/v1/categories')
				.send(categoryData)
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          
		          	res.status.should.be.equal(500);
		          	done();
		          	//nice
		        });
		});

		it('Case2: Create Category - Success', function(done) {
			var categoryData = {
				'name' : 'categoryNewDatxaaxs',
				'categoryImage' : 'mdsgdd',
				'order' : 1
			}

			request(url)
				.post('/api/v1/categories')
				.send(categoryData)
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	res.status.should.be.equal(200);
		          	res.body.data.should.have.property('_id');
	                res.body.data.name.should.equal('categoryNewDatxaaxs');
	                res.body.data.order.should.not.equal(null);
	                res.body.data.categoryImage.should.not.equal(null);
		          	done();
		        });
		});

		it('Case3: Get all the categories', function(done) {
			request(url)
				.get('/api/v1/categories')
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	res.status.should.be.equal(200);
		          	done();
		        });
		});

		it('Case4: Get all the categories based filter - name', function(done) {
			request(url)
				.get('/api/v1/categories?name=categoryNewDatxaaxs')
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	res.status.should.be.equal(200);
		          	//res.body.data._id
		          	done();
		        });
		});

		it('Case5: Get the details of the category based Id', function(done) {
			request(url)
				.get('/api/v1/categories?name=categoryNewDatxaaxs')
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	if (res.body.data && Array.isArray(res.body.data)) {
		          		var data = res.body.data;
		          		var id = data[0]._id;
		          		request(url)
							.get('/api/v1/categories/'+id)
							.end(function(err, res) {
					         	if (err) {
					            	throw err;
					          	}
					          	
					          	res.status.should.be.equal(200);
					          	//res.body.data._id
					          	done();
					        });
		          	} else {
		          		console.log("The category not found case failed");
		          		res.body.data.length.should.not.equal(0);
		          		done();
		          	}
		        });
		});

		it('Case6 & Case7: Get the details of the category and delete it', function(done) {
			request(url)
				.get('/api/v1/categories?name=categoryNewDatxaaxs')
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	if (res.body.data && Array.isArray(res.body.data)) {
		          		var data = res.body.data;
		          		var id = data[0]._id;
		          		request(url)
							.delete('/api/v1/categories/'+id)
							.end(function(err, res) {
					         	if (err) {
					            	throw err;
					          	}
					          	
					          	res.status.should.be.equal(200);
					          	done();
					        });
		          	} else {
		          		console.log("The category not found case failed");
		          		res.body.data.length.should.not.equal(0);
		          		done();
		          	}
		        });
		});

	});
}
